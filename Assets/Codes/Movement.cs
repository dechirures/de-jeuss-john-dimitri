﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Movement : NetworkBehaviour {
	[SyncVar]
	public float speed = 10.0f;
	public Vector3 moveZ;
	public Vector3 scale;
	public int score = 0;


	void Start () 
	{
		moveZ = transform.position;
	}

	/*[Command]

	void CmdDoMove()
	{
		if (isLocalPlayer) {

			moveZ = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			moveZ.z = transform.position.z;
			transform.position = Vector3.MoveTowards (transform.position, moveZ, speed * Time.deltaTime);
		
			if (transform.position.x > 35) {
				transform.position = new Vector3 (35, this.transform.position.y, this.transform.position.z);
			
			
			}
		
			if (transform.position.x < -35) {
				transform.position = new Vector3 (-35, this.transform.position.y, this.transform.position.z);
			
			
			}
		
			if (transform.position.y < -35) {
				transform.position = new Vector3 (this.transform.position.x, -35, this.transform.position.z);
			
			
			}
		
			if (transform.position.y > 35) {
				transform.position = new Vector3 (this.transform.position.x, 35, this.transform.position.z);
			}
			
		}

	}*/

	void Update ()
	{

	
		if (isLocalPlayer) {

		//	CmdDoMove();
			moveZ = transform.position;
			moveZ = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			moveZ.z = transform.position.z;
			transform.position = Vector3.MoveTowards (transform.position, moveZ, speed * Time.deltaTime);

			if (transform.position.x > 35) {
				transform.position = new Vector3 (35, this.transform.position.y, this.transform.position.z);
			
			
			}

			if (transform.position.x < -35) {
				transform.position = new Vector3 (-35, this.transform.position.y, this.transform.position.z);
			
			
			}

			if (transform.position.y < -35) {
				transform.position = new Vector3 (this.transform.position.x, -35, this.transform.position.z);
			
			
			}

			if (transform.position.y > 35) {
				transform.position = new Vector3 (this.transform.position.x, 35, this.transform.position.z);
			
			
			}
		
		} 
		if (!isLocalPlayer) {
		
			//return;
		}
	}


	void OnTriggerEnter (Collider collider)
	{
		if(collider.gameObject.CompareTag("Fodder"))
		{
			score +=1;
			scale = transform.localScale += new Vector3(0.1f, 0.1f, 0);
			Camera.main.orthographicSize+=0.017f;
			if(speed > 3)
			{

			speed-=0.01f;
			}
		}


		 	
	}

	void OnGUI()
	{
		GUI.color = Color.blue;
		GUI.Label(new Rect(0, 0, 1000, 100),"Score: " + score);

		
	}
}
